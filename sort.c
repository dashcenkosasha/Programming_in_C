#include <stdio.h>
#define NMAX 10

void input(int *a, int *n);
void output(int *a, int n);
void sort(int *a, int n);

int main() {
    int n, data[NMAX];
    input(data, &n);
    sort(data, n);
    output(data, n);
    return 0;
}

void input(int *a, int *n) {
    for (int i = 0 ; i < NMAX ; i++) {
        if ((scanf("%d", a + i)) != 1) {
            *n = NMAX +1;
        } else {
            *n = NMAX;
        }
    }
}

void output(int *a, int n) {
    if ( n == NMAX ) {
        for (int i = 0 ; i < n ; i++) {
            printf("%d ", a[i]);
        }
    } else {
        printf("n/a\n");
    }
}

void sort(int *a, int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = (n - 1) ; j > i ; j--) {
            if (a[j-1] > a[j]) {
            int x = a[j-1];
            a[j-1] = a[j];
            a[j] = x;
        }
      }
    }
}
