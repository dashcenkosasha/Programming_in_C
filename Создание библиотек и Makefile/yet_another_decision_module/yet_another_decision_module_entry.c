#include "decision.h"
#include <stdio.h>
#include "../data_libs/data_stat.h"
#include "../data_libs/data_io.h"
#include <stdlib.h>


int main() {
    int n = 1;
    double *data = (double *) malloc (n * sizeof(double));
    input(data, &n);
    if (make_decision(data, n))
        printf("YES");
    else
        printf("NO");
    free(data);
    return 0;
}
