#include "data_process.h"
#include "../data_libs/data_stat.h"
#include "../data_libs/data_io.h"
#include <stdlib.h>

int main() {
    int n = 1;
    double *data = (double *) malloc (n * sizeof(double));
    // Don`t forget to allocate memory !
    input(data, &n);
    if ( n > 0 ) {
       if (normalization(data, n) == 0) {
           n = 0;
        }
    }
    output(data, n);
    free(data);
    return 0;
}
