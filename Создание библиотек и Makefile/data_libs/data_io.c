#include "data_io.h"
#include <stdio.h>
#include <stdlib.h>

void input(double *data, int *n) {
    scanf("%d", n);
    if ( *n > 0 ) {
        data = (double *) realloc (data, *n * sizeof(int));
        for (int i = 0; i < *n; i++) {
            if (scanf("%lf", data + i) !=1) {
               *n = 0;
            }
        }
    }
}

void output(double *data, int n) {
    if ( n > 0 ) {
        for (int i = 0; i < n; i++) {
        printf("%.2lf ", data[i]);
        }
    } else {
        printf("ERROR");
    }
}