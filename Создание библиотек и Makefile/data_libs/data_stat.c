#include "data_stat.h"
#include <math.h>


double max(double *data, int n) {
    double max = 0;
    for (int i = 0; i < n; i++) {
        if (data[i] > max) {
            max = data[i];
        }
    }
    return (max);
}

double min(double *data, int n) {
    double min = 0;
    for (int i = 0; i < n; i++) {
        if (data[i] < min) {
            min = data[i];
        }
    }
    return (min);
}

double mean(double *data, int n) {
    double mean = 0;
    for (int i = 0; i < n; i++) {
        mean = mean + data[i];
        if (i == n) {
             mean = mean / n;
        }
    }
    return (mean);
}

double variance(double *data, int n) {
    double variance = 0;
    double math_expect = mean(data, n);
    for (int i = 0; i < n; i++)
    variance += (data[i] - math_expect) * (data[i] - math_expect);
    variance = variance / n;
    return variance;
}

void sort(double *data, int n) {
      for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (data[j] > data[j + 1]) {
                double x = data[+ j];
                data[+ j] = data[+ j + 1];
                data[+j +1] = x;
            }
        }
    }
}
