#include "../data_module/data_process.h"
#include "../yet_another_decision_module/decision.h"
#include "../data_libs/data_stat.h"
#include "../data_libs/data_io.h"
#include <stdlib.h>
#include <stdio.h>



int main() {
    int n = 1;
    double *data = (double *) malloc (n * sizeof(double));

    printf("LOAD DATA...\n");
    input(data, &n);
    if (n > 0) {
        printf("RAW DATA:\n\t");
        output(data, n);

        printf("\nNORMALIZED DATA:\n\t");
        normalization(data, n);
        output(data, n);

        printf("\nSORTED NORMALIZED DATA:\n\t");
        sort(data, n);
        output(data, n);

        printf("\nFINAL DECISION:\n\t");
        if (make_decision(data, n)) {
            printf("YES");
        } else {
            printf("NO");
        }
    } else {
        output(data, n);
    }
    free(data);
    return 0;
}
