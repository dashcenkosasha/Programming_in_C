#include <stdio.h>
#include <stdlib.h>

void input(int *a, int *n);
void output(int *a, int n);
void sort(int *a, int n);

int main() {
    int n = 0,
        *data = (int*) malloc(n * sizeof(int));
    input(data, &n);
    if ( n == 0 ) {
        printf("n/a\n");
    } else {
        sort(data, n);
        output(data, n);
    }
    free(data);
    return 0;
}

void input(int *a, int *n) {
    if ((scanf("%d", n)) != 1) {
        *n = 0;
    } else {
    for (int i = 0 ; i < *n; i++) {
        if ((scanf("%d", a + i)) != 1) {
            *n = 0;
        }
    }
    }
}

void output(int *a, int n) {
    if ( n > 0 ) {
        for (int i = 0 ; i < n ; i++) {
            printf("%d", a[i]);
            if ( i != n - 1 ) {
                printf(" ");
            }
        }
    } else {
        printf("n/a\n");
    }
}

void sort(int *a, int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = (n - 1) ; j > i ; j--) {
            if (a[j-1] > a[j]) {
            int x = a[j-1];
            a[j-1] = a[j];
            a[j] = x;
        }
      }
    }
}
